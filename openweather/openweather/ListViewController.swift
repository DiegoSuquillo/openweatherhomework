//
//  ListViewController.swift
//  openweather
//
//  Created by USRDEL on 17/7/18.
//  Copyright © 2018 Diego Suquillo. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var weatherList: UITableView!
    var weatherManager = WeatherManager()
    
    var weatherInfo:(weatherManager: WeatherManager, index: Int)?
    override func viewDidLoad() {
        super.viewDidLoad()
        weatherManager.updateArray()
        self.weatherList.reloadData()
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherManager.weatherItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = weatherList.dequeueReusableCell(withIdentifier: "celda", for: indexPath) as! InfoTableViewCell
        cell.cityLabel.text = self.weatherManager.weatherItems[indexPath.row].city
        cell.descriptionLabel.text = self.weatherManager.weatherItems[indexPath.row].weatherDescription
        
        return cell
    }
}
