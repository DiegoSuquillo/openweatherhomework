//
//  WeatherManager.swift
//  openweather
//
//  Created by USRDEL on 17/7/18.
//  Copyright © 2018 Diego Suquillo. All rights reserved.
//

import Foundation
import RealmSwift

class WeatherManager {
    var weatherItems:[WeatherRealm] = []
    var realm:Realm
    
    init() {
        realm = try! Realm()
        print(realm.configuration.fileURL)
    }
    
    func addItem(city: String, weatherDescription: String?) {
        let weather = WeatherRealm()
        weather.id = "\(UUID())"
        weather.city = city
        weather.weatherDescription = weatherDescription
        
        try! realm.write {
            realm.add(weather)
        }
    }
    
    func updateArray() {
        weatherItems = Array(realm.objects(WeatherRealm.self))
    }
}
