//
//  ViewController.swift
//  openweather
//
//  Created by Diego Suquillo on 6/30/18.
//  Copyright © 2018 Diego Suquillo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var inputTextCity: UITextField!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var weatherManager = WeatherManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func pressedButtonRequest(_ sender: Any) {
        let city: String = inputTextCity.text ?? "tokyo"
        let key: String = "81bd160665598a6ad1c0d3fc9f1152e1"
        let urlString =  "https://api.openweathermap.org/data/2.5/weather?q=" + city + "&appid=" + key
        let url = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) {
            data, response, error in
            
            guard let data = data else {
                print("Error NO data")
                return
            }
            
            guard let weatherInfo = try? JSONDecoder().decode(WeatherInfo.self, from: data) else {
                print("Error decoding")
                return
            }
            
            DispatchQueue.main.async {
                self.labelDescription.text = "\(weatherInfo.weather[0].description)"
                
                let iconImg: String = ("\(weatherInfo.weather[0].icon)" + ".png")
                let imageUrlString = "https://openweathermap.org/img/w/" + iconImg
                let imageUrl: URL = URL ( string: imageUrlString )!
                
                let imageData:NSData = NSData(contentsOf: imageUrl)!
                let imageView = UIImageView(frame: CGRect(x:0, y:0, width:200, height:200))
                imageView.center = self.view.center
                let image = UIImage(data: imageData as Data)
                self.imageView.image = image
                self.imageView.contentMode = UIViewContentMode.scaleAspectFit
                self.view.addSubview(imageView)
                
                self.weatherManager.addItem(city: city, weatherDescription: weatherInfo.weather[0].description)
                
            }
            
        }
        task.resume()
    }
    
    
    @IBAction func showHistoryButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "toList", sender: self)
    }
    
}

