//
//  Weather.swift
//  openweather
//
//  Created by Diego Suquillo on 6/30/18.
//  Copyright © 2018 Diego Suquillo. All rights reserved.
//

import Foundation
import RealmSwift

struct WeatherInfo: Decodable {
    let weather: [Weather]
}

struct Weather: Decodable {
    let id:Int
    let description:String
    let icon:String
}

class WeatherRealm: Object {
    @objc dynamic var id: String?
    @objc dynamic var city: String?
    @objc dynamic var weatherDescription: String?
    override static func primaryKey() -> String? {
        return "id"
    }
}
